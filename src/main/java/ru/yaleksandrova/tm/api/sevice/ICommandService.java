package ru.yaleksandrova.tm.api.sevice;

import ru.yaleksandrova.tm.model.Command;

public interface ICommandService {

    Command[] getCommands();

}
