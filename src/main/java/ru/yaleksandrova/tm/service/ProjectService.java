package ru.yaleksandrova.tm.service;

import ru.yaleksandrova.tm.api.repository.IProjectRepository;
import ru.yaleksandrova.tm.api.sevice.IProjectService;
import ru.yaleksandrova.tm.enumerated.Status;
import ru.yaleksandrova.tm.model.Project;
import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(final String name) {
        if (name == null || name.isEmpty()) return;
        final Project project = new Project();
        project.setName(name);
        projectRepository.add(project);
    }

    @Override
    public void create(final String name, final String description) {
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
    }

    @Override
    public void add(final Project project) {
        if (project == null) return;
        projectRepository.add(project);
    }

    @Override
    public void remove(final Project project) {
        if (project == null) return;
        projectRepository.remove(project);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Project findById(final String id) {
        if(id == null || id.isEmpty()) return null;
        return projectRepository.findById(id);
    }

    @Override
    public Project findByName(String name) {
        if(name == null || name.isEmpty()) return null;
        return projectRepository.findByName(name);
    }

    @Override
    public Project findByIndex(Integer index) {
        if(index == null || index < 0) return null;
        return projectRepository.findByIndex(index);
    }

    @Override
    public Project updateById(String id, String name, String description) {
        if (id == null || id.isEmpty() || name == null || name.isEmpty())
            return null;
        final Project project = findById(id);
        if (project == null)
            return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0 || name == null || name.isEmpty())
            return null;
        final Project project = findByIndex(index);
        if (project == null)
            return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project removeById(String id) {
        if (id == null || id.isEmpty())
            return null;
        return projectRepository.removeById(id);
    }

    @Override
    public Project removeByName(String name) {
        if (name == null || name.isEmpty())
            return null;
        return projectRepository.removeByName(name);
    }

    @Override
    public Project removeByIndex(Integer index) {
        if (index == null || index < 0)
            return null;
        return projectRepository.removeByIndex(index);
    }

    @Override
    public Project startByIndex(Integer index) {
        if (index == null || index < 0)
            return null;
        return projectRepository.startByIndex(index);
    }

    @Override
    public Project startByName(String name) {
        if (name == null || name.isEmpty())
            return null;
        return projectRepository.startByName(name);
    }

    @Override
    public Project startById(String id) {
        if (id == null || id.isEmpty())
            return null;
        return projectRepository.startById(id);
    }

    @Override
    public Project finishById(String id) {
        if (id == null || id.isEmpty())
            return null;
        return projectRepository.finishById(id);
    }

    @Override
    public Project finishByIndex(Integer index) {
        if (index == null || index < 0)
            return null;
        return projectRepository.finishByIndex(index);
    }

    @Override
    public Project finishByName(String name) {
        if (name == null || name.isEmpty())
            return null;
        return projectRepository.finishByName(name);
    }

    @Override
    public Project changeStatusById(String id, Status status) {
        if (id == null || id.isEmpty()) return null;
        if (status == null) return null;
        return projectRepository.changeStatusById(id, status);
    }

    @Override
    public Project changeStatusByIndex(Integer index, Status status) {
        if (index == null || index < 0) return null;
        if (status == null) return null;
        return projectRepository.changeStatusByIndex(index, status);
    }

    @Override
    public Project changeStatusByName(String name, Status status) {
        if (name == null || name.isEmpty()) return null;
        if (status == null) return null;
        return projectRepository.changeStatusByName(name, status);
    }
}
