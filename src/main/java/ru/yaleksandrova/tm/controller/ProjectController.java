package ru.yaleksandrova.tm.controller;

import ru.yaleksandrova.tm.api.controller.IProjectController;
import ru.yaleksandrova.tm.api.sevice.IProjectService;
import ru.yaleksandrova.tm.enumerated.Status;
import ru.yaleksandrova.tm.model.Project;
import ru.yaleksandrova.tm.util.ApplicationUtil;
import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showProjects() {
        System.out.println("[LIST PROJECTS]");
        final List<Project> projects = projectService.findAll();
        for (Project project: projects) System.out.println(project);
        System.out.println("[OK]");
    }

    @Override
    public void clearProject() {
        System.out.println("[CLEAR PROJECT]");
        projectService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = ApplicationUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = ApplicationUtil.nextLine();
        this.projectService.create(name, description);
        System.out.println("[OK]");
    }

    @Override
    public void showById() {
        System.out.println("[ENTER ID]");
        final String id = ApplicationUtil.nextLine();
        final Project project = projectService.findById(id);
        if (project==null){
            System.out.println("[Error! No matches found]");
            return;
        }
        showProject(project);
    }

    @Override
    public void showByIndex() {
        System.out.println("[ENTER INDEX]");
        final int index = Integer.parseInt(ApplicationUtil.nextLine());
        final Project project = projectService.findByIndex(index);
        if (project==null){
            System.out.println("[Error! No matches found]");
            return;
        }
        showProject(project);
    }

    @Override
    public void showProject(Project project) {
        if (project == null)
            return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + project.getStatus().getDisplayName());
    }

    @Override
    public void removeById() {
        System.out.println("ENTER ID:");
        final String id = ApplicationUtil.nextLine();
        final Project project = projectService.removeById(id);
        if (project == null) {
            System.out.println("[Error! No matches found]");
            return;
        }
        System.out.println("[Project deleted]");
        showProject(project);
    }

    @Override
    public void removeByIndex() {
        System.out.println("[ENTER INDEX]");
        final int index = Integer.parseInt(ApplicationUtil.nextLine());
        final Project project = projectService.removeByIndex(index);
        if (project==null){
            System.out.println("[Error! No matches found]");
            return;
        }
        System.out.println("[Project deleted]");
        showProject(project);
    }

    @Override
    public void removeByName() {
        System.out.println("[ENTER NAME]");
        final String name = ApplicationUtil.nextLine();
        final Project project = projectService.removeByName(name);
        if (project==null){
            System.out.println("[Error! No matches found]");
            return;
        }
        System.out.println("[Project deleted]");
        showProject(project);
    }

    @Override
    public void updateById() {
        System.out.println("ENTER ID:");
        final String id = ApplicationUtil.nextLine();
        final Project project = projectService.findById(id);
        if (project == null) {
            System.out.println("[Error! No matches found]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = ApplicationUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = ApplicationUtil.nextLine();
        final Project projectUpdatedId = projectService.updateById(id, name, description);
        if (projectUpdatedId == null) {
            return;
        }
        System.out.println("[Project updated]");
    }

    @Override
    public void updateByIndex() {
        System.out.println("[ENTER INDEX]");
        final int index = Integer.parseInt(ApplicationUtil.nextLine());
        final Project project = projectService.findByIndex(index);
        if (project == null) {
            System.out.println("[Error! No matches found]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = ApplicationUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = ApplicationUtil.nextLine();
        final Project projectUpdatedIndex = projectService.updateByIndex(index, name, description);
        if (projectUpdatedIndex == null) {
            return;
        }
        System.out.println("[Project updated]");
    }

    @Override
    public void startById() {
        System.out.println("[ENTER ID]");
        final String id = ApplicationUtil.nextLine();
        final Project project = projectService.startById(id);
        if (project == null) {
            System.out.println("Incorrect value!");
        }
    }

    @Override
    public void startByIndex() {
        System.out.println("[ENTER INDEX]");
        final Integer index = Integer.parseInt(ApplicationUtil.nextLine());
        final Project project = projectService.startByIndex(index);
        if (project == null) {
            System.out.println("Incorrect value!");
        }
    }

    @Override
    public void startByName() {
        System.out.println("[ENTER NAME]");
        final String name = ApplicationUtil.nextLine();
        final Project project = projectService.startByName(name);
        if (project == null) {
            System.out.println("Incorrect value!");
        }
    }

    @Override
    public void finishById() {
        System.out.println("[ENTER ID]");
        final String id = ApplicationUtil.nextLine();
        final Project project = projectService.finishById(id);
        if (project == null) {
            System.out.println("Incorrect value!");
        }
    }

    @Override
    public void finishByIndex() {
        System.out.println("[ENTER INDEX]");
        final Integer index = Integer.parseInt(ApplicationUtil.nextLine());
        final Project project = projectService.finishByIndex(index);
        if (project == null) {
            System.out.println("Incorrect value!");
        }
    }

    @Override
    public void finishByName() {
        System.out.println("[ENTER NAME]");
        final String name = ApplicationUtil.nextLine();
        final Project project = projectService.finishByName(name);
        if (project == null) {
            System.out.println("Incorrect value!");
        }
    }

    @Override
    public void changeStatusById() {
        System.out.println("[ENTER ID]");
        final String id = ApplicationUtil.nextLine();
        System.out.println("[ENTER STATUS]");
        final String statusValue = ApplicationUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Project project = projectService.changeStatusById(id, status);
        if (project == null) {
            System.out.println("Incorrect value!");
        }
    }

    @Override
    public void changeStatusByIndex() {
        System.out.println("[ENTER INDEX]");
        final Integer index = Integer.parseInt(ApplicationUtil.nextLine());
        System.out.println("[ENTER STATUS]");
        final String statusValue = ApplicationUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Project project = projectService.changeStatusByIndex(index, status);
        if (project == null) {
            System.out.println("Incorrect value!");
        }
    }

    @Override
    public void changeStatusByName() {
        System.out.println("[ENTER NAME]");
        final String name = ApplicationUtil.nextLine();
        System.out.println("[ENTER STATUS]");
        final String statusValue = ApplicationUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Project project = projectService.changeStatusByName(name, status);
        if (project == null) {
            System.out.println("Incorrect value!");
        }
    }

}
